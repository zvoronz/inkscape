// SPDX-License-Identifier: GPL-2.0-or-later
#ifndef SEEN_SP_CUSTOM_H
#define SEEN_SP_CUSTOM_H

/*
 * SVG <custom> implementation
 *
 * Authors:
 *   Jeff Schiller <codedread@gmail.com>
 *
 * Copyright (C) 2008 Jeff Schiller
 *
 * Released under GNU GPL v2+, read the file 'COPYING' for more information.
 */

#include "sp-object.h"

#define SP_CUSTOM(obj) (dynamic_cast<SPCustom*>((SPObject*)obj))
#define SP_IS_CUSTOM(obj) (dynamic_cast<const SPCustom*>((SPObject*)obj) != NULL)

class SPCustom : public SPObject {
public:
	SPCustom();
	~SPCustom() override;

	Inkscape::XML::Node* write(Inkscape::XML::Document *xml_doc, Inkscape::XML::Node *repr, unsigned int flags) override;
};

#endif
