// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * SVG <custom> implementation
 *
 * Authors:
 *   Jeff Schiller <codedread@gmail.com>
 *
 * Copyright (C) 2008 Jeff Schiller
 *
 * Released under GNU GPL v2+, read the file 'COPYING' for more information.
 */

#include "sp-custom.h"
#include "xml/repr.h"

SPCustom::SPCustom() : SPObject() {
}

SPCustom::~SPCustom() = default;

Inkscape::XML::Node* SPCustom::write(Inkscape::XML::Document *xml_doc, Inkscape::XML::Node *repr, guint flags) {
	SPCustom* object = this;

    if (!repr) {
        repr = object->getRepr()->duplicate(xml_doc);
    }

    SPObject::write(xml_doc, repr, flags);

    return repr;
}

